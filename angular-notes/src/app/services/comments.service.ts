import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment";

// Requirement 1: set up header value to the content-type of application/json.

const httpOptions = {

  headers: new HttpHeaders({"Content-Type": "application/json"})

}

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  commentsUrl: string = "https://jsonplaceholder.typicode.com/comments";

  constructor(private httpClient: HttpClient) { }

  getComments() : Observable<Comment[]> {

    return this.httpClient.get<Comment[]>(this.commentsUrl);

  }

  // Requirement 2: create a new observable method to POST a return request

  saveComment(newComment: Comment) : Observable<Comment> {

    return this.httpClient.post<Comment>(this.commentsUrl, newComment, httpOptions);

  }

  updateComment(commentUpdated: Comment) : Observable<Comment> {

    let url = `${this.commentsUrl}/${commentUpdated.id}`;

    return this.httpClient.put<Comment>(url, commentUpdated, httpOptions)

  }

  deleteComment(commentDeleted: Comment | number) : Observable<Comment> {

    let id = typeof commentDeleted === "number" ? commentDeleted : commentDeleted.id;

    let url = `${this.commentsUrl}/${id}`;

    return this.httpClient.delete<Comment>(url, httpOptions);

  }

}
