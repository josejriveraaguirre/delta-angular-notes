import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostHttpClient} from "../models/PostHttpClient";

// set up our headers value to the content-type of application/json. This is specific to this example, not generally needed.

const httpOptions = {

  headers: new HttpHeaders({"Content-Type": "application/json"})

}

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  // PROPERTIES

  //set URL as a property

  postsUrl: string ="https://jsonplaceholder.typicode.com/posts";


  // inject the HttpClientModule as a dependency

  constructor(private httpClient: HttpClient) { }

//  create a method that will make aour GET request to the postsUrl

  getPosts() : Observable<PostHttpClient[]> {

    //returning all the data that comes with our postsUrl property

    return this.httpClient.get<PostHttpClient[]>(this.postsUrl);
  }

//   create a method to make a post request to the postsUrl property

  savePost(newPost: PostHttpClient) : Observable<PostHttpClient> {

    return this.httpClient.post<PostHttpClient>(this.postsUrl, newPost, httpOptions);

  }

  updatePost(postUpdated: PostHttpClient) : Observable<PostHttpClient> {

    let url = `${this.postsUrl}/${postUpdated.id}`;

    return this.httpClient.put<PostHttpClient>(url, postUpdated, httpOptions)

  }

  // method to make a DELETE request to the url

  deletePost(postDeleted: PostHttpClient | number) : Observable<PostHttpClient> {

    let id = typeof postDeleted === "number" ? postDeleted : postDeleted.id;

    let url = `${this.postsUrl}/${id}`;

    return this.httpClient.delete<PostHttpClient>(url, httpOptions);

  }
}
