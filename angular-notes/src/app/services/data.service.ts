import { Injectable } from '@angular/core';
import {DCHero} from "../models/DCHero";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //PROPERTIES

  //initialize character property and assign to DCHero interface

  characters: DCHero[] = [];

  constructor() {
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address: {
          street: "50 Wayne Manor",
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/70-batman.jpg",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("09/01/1975 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Diana",
        lastName: "Prince",
        age: 5000,
        address: {
          street: "150 Main St.",
          city: "Los Angeles",
          state: "CA"
        },
        img: "../assets/img/720-wonder-woman.jpg",
        isActive: true,
        balance: 14000000,
        memberSince: new Date("07/11/2005 8:30:00"),
        hide: false
      },
      {
        persona: "Aquaman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/38-aquaman.jpg",
        // img: "../../../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Flash",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/265-flash-ii.jpg",
        // img: "../../../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Cyborg",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/194-cyborg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      }
    ]; // end of array
  } // end of constructor

//  METHODS
//  CREATE A METHOD THAT WILL RETAIN AN ARRAY OF this.characters USING OBSERVABLES
//  OBSERVABLES
  /*
  support passing messages/data between parts of your app
  used freq and is a technique for event handling, asynchronous prog and handling multiple values
   */

  getCharacters() : Observable<DCHero[]> {
    console.log("Getting characters from Data Service");
    return of(this.characters);
  }

} // end of class
