export interface Movie {
  title: string,
  img?: string,
  yearReleased?: number,
  actors?: string,
  director?: string,
  genre?: string,
  rating?: string,
  awards?: any
}
