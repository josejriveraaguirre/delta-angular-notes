import {Component, OnInit, ViewChild} from '@angular/core';
import {DCHero} from "../../models/DCHero";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  characters: DCHero[] = [];
  currentClasses: {} = {};
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;

 // character empty object

  character: DCHero = {
    persona: '',
    firstName: '',
    lastName: '',
    age: 0,
    address: {
      street: '',
      city: '',
      state: ''
    },
    img: '',
    isActive: true,
    memberSince: '',
    hide: true
  }

// submit lesson

  @ViewChild("characterForm") form: any;

  /*
  Calling ViewChild and passed in the name of our form - making it our "form identifier".
   */

  // inject the data service in the constructor. This is the sole purpose of the constructor.

  constructor(private dataService: DataService) { }

  /*
  Dependency Injection
  private - can only be used in this class
  dataService - variable name for our DataService
  DataService - setting our variable to the DataService that we brought in.
  Now we should be able to access any method inside of our DataService.
   */

  ngOnInit(): void {

    this.setCurrentClasses();
    this.setCurrentStyle();

    // access the getCharacters() method tht's inside of our DataService

    this.dataService.getCharacters().subscribe(data => {
      this.characters = data;
    });

  //  whenever we are accessing an observable method we need to subscribe to it.

  } //end of ngOnInit()

  //METHODS
  //example of ngClass
  setCurrentClasses() {
    this.currentClasses = {
      "btn-success": this.enableAddCharacter
    }
  }

  //example of ngStyle

  setCurrentStyle() {

    //call and update the property currentStyle

    this.currentStyle = {

      "padding-top": "60px",
      "text-decoration": "underline"

    }


  }

//  toggle indiv char info

  toggleInfo(character: any) {
    console.log("Toggle info clicked");
    character.hide =!character.hide;
  }

  // addCharacter() {
  //
  //   this.characters.unshift(this.character);
  //
  //   //reset the form
  //
  //   this.character = {
  //     persona: "",
  //     firstName: "",
  //     lastName: "",
  //     age: 0,
  //     address: {
  //       street: '',
  //       city: '',
  //       state: ''
  //     },
  //     img: '',
  //     isActive: true,
  //     memberSince: '',
  //     hide: true
  //   }
  //
  // }

  onSubmit({value, valid} : {value: DCHero, valid: boolean}) {

   if (!valid) {

     alert("Form is not valid");

   } else {
     value.isActive = true;
     value.memberSince = new Date();
     value.hide = false;
     console.log(value);
   }

   this.characters.unshift(value);

   this.form.reset();

  }

}

