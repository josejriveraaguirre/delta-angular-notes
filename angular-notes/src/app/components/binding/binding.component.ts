import { Component, OnInit } from '@angular/core';
import {DCHero} from "../../models/DCHero";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  characters: DCHero[] = [];
  currentClasses: {} = {};
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;

  constructor() { }

  ngOnInit(): void {

    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
     },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address: {
          street: "50 Wayne Manor",
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/70-batman.jpg",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("09/01/1975 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Diana",
        lastName: "Prince",
        age: 5000,
        address: {
          street: "150 Main St.",
          city: "Los Angeles",
          state: "CA"
        },
        img: "../assets/img/720-wonder-woman.jpg",
        isActive: true,
        balance: 14000000,
        memberSince: new Date("07/11/2005 8:30:00"),
        hide: false
      },
      {
        persona: "Aquaman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/38-aquaman.jpg",
        // img: "../../../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Flash",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/265-flash-ii.jpg",
        // img: "../../../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Cyborg",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: "27 Smalville",
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/194-cyborg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      }

    ]; //end of array

    this.setCurrentClasses();
    this.setCurrentStyle()

  } //end of ngOnInit()

  //METHODS
  //example of ngClass
  setCurrentClasses() {
    this.currentClasses = {
      "btn-success": this.enableAddCharacter
    }
  }

  //example of ngStyle

  setCurrentStyle() {

    //call and update the property currentStyle

    this.currentStyle = {

      "padding-top": "60px",
      "text-decoration": "underline"

    }


  }

//  toggle indiv char info

  toggleInfo(character: any) {
    console.log("Toggle info clicked");
    character.hide =!character.hide;
  }


} //end of class
