import { Component, OnInit } from '@angular/core';
import {Athlete} from "../../models/Athlete";

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  //PROPERTIES

  athletes: Athlete[] = []; //pointing to our Athlete interface

  constructor() { }

  ngOnInit(): void {

    // "this." needed to access a property inside a method

    this.athletes = [
      {
        name: "Michael Jordan",
        team: "Chicago Bulls",
        jerseyNo: 23,
        stillPlaying: false
      },
      {
        name: "LeBron James",
        team: "Los Angeles Lakers",
        jerseyNo: 23,
        stillPlaying: true
      },
      {
        name: "Lionel Messi",
        team: "Barcelona",
        jerseyNo: 10,
        stillPlaying: true
      },
      {
        name: "Alex Morgan",
        team: "Orlando Pride",
        jerseyNo: 13,
        stillPlaying: true
      }
    ] //end of athletes array

  } //end of ngOnInit()

  //METHOD

  showStatus() {
    return "This athlete is currently playing";
  }

}
