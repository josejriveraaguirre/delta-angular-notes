import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from "../../models/Comment";
import {CommentsService} from "../../services/comments.service";

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  //PROPERTIES

  // Requirement 4: Create two properties

  comment: Comment = {

    id: 0,
    name: "",
    email: "",
    body: ""

  }

  @Output() newComment: EventEmitter<Comment> = new EventEmitter()

  @Output() updatedComment: EventEmitter<Comment> = new EventEmitter<Comment>()

  @Input() currentComment: Comment = {

    id: 0,
    name: "",
    email: "",
    body: ""

  }

  @Input() isEdit: boolean = false

  // Requirement 5a: Create a method and subscribe to the service

  constructor(private commentsService: CommentsService ) { }

  ngOnInit(): void {

  }

  // Requirement 5b: Create a method and subscribe to the service

  addComment(name: string, email: string, body: string) {

    if (!name || !email || !body) {

      alert("We really want to know about your experience with us! Would you please add your comments?")

    } else {

      console.log(name, email, body);

      // now use the dependency injection made on the constructor

      this.commentsService.saveComment({name, email, body} as Comment).subscribe(data => {

        this.newComment.emit(data);

      })

    }

    this.currentComment = {

      id: 0,
      name: "",
      email: "",
      body: ""

    }

  }

  updateComment() {

    console.log("Updating comment...");

    this.commentsService.updateComment(this.currentComment).subscribe(data => {

      this.isEdit = false;

      this.updatedComment.emit(data);

    })

    this.currentComment = {

      id: 0,
      name: "",
      email: "",
      body: ""

    }

  }

}
