import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  //PROPERTIES

  showForm: boolean = true;

  constructor() { }

  ngOnInit(): void {

    this.showForm = false;

  }

//  METHODS

  toggleForm() {

    console.log("Toggle form button was clicked");

    this.showForm = !this.showForm;

    console.log(this.showForm);

  }

//  method for the different types of mouse events

  triggerEvent(event: any) {

    console.log(event.type);

  }

}
