import { Component, OnInit } from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {

  //properties

  posts: PostHttpClient[] = []

  selectedPost: PostHttpClient = {

    id:0,
    title: "",
    body: ""

  }

  postEdit: boolean = false


  // inject our service as a dependency

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {

  //  subscribe to our Obsrvable method that's in our PostsService

    this.postsService.getPosts().subscribe(data => {

      console.log(data);

      // reassigning our property 'posts'

      this.posts = data;

    })

  }

//  create onSubmitPost() to add new post to the this.posts array

  onSubmitPost(post: PostHttpClient) {

    this.posts.unshift(post);

  }

  onUpdatedPost(editPost: PostHttpClient) {

    this.posts.forEach((current, index) => {

      if (editPost.id === current.id) {

        this.posts.splice(index, 1);

        this.posts.unshift(editPost);

        this.postEdit = false;

        this.selectedPost = {

          id: 0,
          title: "",
          body: ""

        }

      }

    })

  }

  editPost(post: PostHttpClient) {

    this.selectedPost = post;

    this.postEdit = true;
  }

//  create a method to subscribe to the observable delete method in the service

  removePost(postToBeDeleted: PostHttpClient) {

    if (confirm("Are you sure?")) {

      this.postsService.deletePost(postToBeDeleted.id).subscribe(() => {

        this.posts.forEach((current, index) => {

          if (postToBeDeleted.id === current.id) {

            this.posts.splice(index,1);
          }
        })
      })
    }

  }

}
