import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {

  message: boolean = true;
  members: Member[] = [];


  constructor() { }

  ngOnInit(): void {

     this.loadingMsg()

     this.members = [
      {
        firstName: "Joseph R.",
        lastName: "Biden",
        username: "POTUS",
        memberNo: 1
      },
      {
        firstName: "Kamala",
        lastName: "Harris",
        username: "VPOTUS",
        memberNo: 2
      },
      {
        firstName: "Nancy",
        lastName: "Pelosi",
        username: "SHR",
        memberNo: 3
      },
      {
        firstName: "Patrick",
        lastName: "Leahy",
        username: "PROTEM",
        memberNo: 4
      },
      {
        firstName: "Anthony",
        lastName: "Blinken",
        username: "SECSTATE",
        memberNo: 5
      },
    ]

  }

  loadingMsg() {

    setTimeout(() => {
      this.message = false;
    },5000)
  }

}
