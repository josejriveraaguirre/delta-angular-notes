import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  //PROPERTIES

  goodWeather: string = "sunny and cool.";
  badWeather: string = "cold and snow storm.";

  constructor() { }

  ngOnInit(): void {

    // CALLS

    // this.displayGoodWeatherMessage();
    this.dislayBadWeatherMessage();

  }
//METHODS

  displayGoodWeatherMessage() {
    alert(`Today's weather is ${this.goodWeather}`);
  }

  dislayBadWeatherMessage() {
    alert(`Bundle up, charge your phones, because today's weather is ${this.badWeather}`);
  }

}
