import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies: Movie[] = [];
  // mpaRating: string = "";
  // mpaRating1: string = "";
  // i: number = 0;

  constructor() { }

  ngOnInit(): void {

    this.movies = [

      {
        title: "Indiana Jones and the Raiders of the Lost Ark",
        img: "../assets/img/movies/Raiders_of_the_Lost_Ark.jpg",
        yearReleased: 1981,
        actors: "Harrison Ford, Karen Allen, Paul Freeman",
        director: "Steven Spielberg",
        genre: "Action,Adventure",
        rating: "PG",
        awards: ["Best Art Direction", "Best Sound", "Best Film Editing", "Best Visual Effects", "Special Achievement Award for Sound Effects Editing"]

     },
      {
        title: "Indiana Jones and the Temple of Doom",
        img: "../assets/img/movies/Indiana_Jones_and_the_Temple_of_Doom_PosterB.jpg",
        yearReleased: 1984,
        actors: "Harrison Ford, Kate Capshaw, Ke Huy Quan",
        director: "Steven Spielberg",
        genre: "Action,Adventure",
        rating: "PG",
        awards: ["Best Visual Effects"]

      },
      {
        title: "Indiana Jones and the Last Crusade",
        img: "../assets/img/movies/Indiana_Jones_and_the_Last_Crusade.png",
        yearReleased: 1989,
        actors: "Harrison Ford, Sean Connery, Alison Doody",
        director: "Steven Spielberg",
        genre: "Action,Adventure",
        rating: "PG-13",
        awards: ["Best Sound Effects Editing"]

      },
      {
        title: "Indiana Jones and the Kingdom of the Crystal Skull",
        img: "../assets/img/movies/Kingdomofthecrystalskull.jpg",
        yearReleased: 2008,
        actors: "Harrison Ford, Cate Blanchett, Shia LaBeouf",
        director: "Steven Spielberg",
        genre: "Action,Adventure",
        rating: "PG-13",
        awards: []

      }

    ];

    // this.ratingMsg();
    // this.ratingMsg1();

  }

  // ratingMsg() {
  //
  //   for(this.i = 0; this.i < this.movies.length; this.i++) {
  //
  //     switch (this.movies[this.i].rating) {
  //
  //       case 'PG':
  //         this.mpaRating = " – Parental Guidance Suggested";
  //         break;
  //
  //       default:
  //         this.mpaRating = '';
  //         break;
  //
  //     }
  //
  //   }
  //
  // }
  // ratingMsg1() {
  //
  //   for(this.i = 0; this.i < this.movies.length; this.i++) {
  //
  //     switch (this.movies[this.i].rating) {
  //
  //       case 'PG-13':
  //         this.mpaRating1 = " – Parents Strongly Cautioned";
  //         break;
  //
  //       default:
  //         this.mpaRating1 = '';
  //         break;
  //
  //     }
  //
  //   }
  //
  // }


}
