import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delta',
  templateUrl: './delta.component.html',
  styleUrls: ['./delta.component.css']
})
export class DeltaComponent implements OnInit {

  //PROPERTIES - an attribute of the component

  firstName: string = "Delta";
  lastName: string = "Cohort";
  year: number = 2021;


//called when component is initialized (used)
// used for dependency injections - connection from the service(s)

  constructor() { }

  //called when component is initialized
  //used for reassigning properties and calling methods

  ngOnInit(): void {

    console.log("Hello from DeltaComponent...");

    console.log(`${this.firstName} ${this.lastName}`);

    //In order to access prop we need to use the "this" keyword

    this.greeting();

    //in order to call a method, use the "this" keyword

  }

  //METHODS - function inside of a component's class

  greeting() {

    alert("Hello there " + this.firstName);

    this.displayLastName();

    alert("They year is " + this.year);

    //*NOTE: to access properties in a method, use this keyword
  }

  displayLastName() {

    alert("Last name is " + this.lastName);

  }

} // end of class
