import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  post: PostHttpClient = {
    id: 0,
    title: "",
    body: ""
  };

  @Output() newPost: EventEmitter<PostHttpClient> = new EventEmitter();

  @Output() updatedPost: EventEmitter<PostHttpClient> = new EventEmitter();

  @Input() currentPost: PostHttpClient = {

    id: 0,
    title: "",
    body: ""

  }

  @Input() isEditing: boolean = false;

  constructor(private postService: PostsService) { }

  ngOnInit(): void {

  }

  // create a method named addPost() by using the PostService savePost()
  //  and attaching the event emitter

    addPost(title: any, body: any) {

      if (!title || !body) {

        alert("Please complete the form.")

      } else {

        console.log(title, body);

        this.postService.savePost({title, body} as PostHttpClient).subscribe(data => {

          this.newPost.emit(data);

        })

      }

      this.currentPost = {

        id: 0,
        title: "",
        body: ""

      }

    }



  updatePost() {

      console.log("Updating post...");

      this.postService.updatePost(this.currentPost).subscribe(data => {

        this.isEditing = false;

        this.updatedPost.emit(data);

      })

    this.currentPost = {

      id: 0,
      title: "",
      body: ""

    }

  }

}
