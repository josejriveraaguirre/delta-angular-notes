import { Component, OnInit } from '@angular/core';
import {MarvelHero} from "../../models/MarvelHero";

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {

  //PROPERTIES

  heroes: MarvelHero[] = [];

  //property for the new character - sets all properties empty

  character: MarvelHero = {
    persona: "",
    firstName: "",
    lastName: "",
    age: 0
  };

  constructor() { }

  ngOnInit(): void {

  //  add data

    this.heroes = [
      {
        persona: "Captain America",
        firstName: "Steve",
        lastName: "Rogers",
        age: 77
      },
      {
        persona: "Spider-Man",
        firstName: "Peter",
        lastName: "Parker",
        age: 17
      }
    ]

  } // END OF NGoNiNIT

//  METHODS

  addCharacter() {

    this.heroes.unshift(this.character);

    //reset the form

    this.character = {
      persona: "",
      firstName: "",
      lastName: "",
      age: 0
    }

  }

}
