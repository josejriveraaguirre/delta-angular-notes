import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {

  num1: number = 1;
  num2: number = 2;
  i: number = 0;

  constructor() { }

  ngOnInit(): void {

    console.log(this.sum());
    console.log(this.difference());
    console.log(this.product());
    console.log(this.quotient());
    this.numbers();

  }

  sum() {
    return this.num1 + this.num2
  }

  difference() {
    return this.num1 - this.num2
  }

  product() {
    return this.num1 * this.num2
  }

  quotient() {
    return this.num1 / this.num2
  }

  numbers() {

    for (this.i=1; this.i <= 100; this.i++) {

      if (this.i % 3 === 0 && this.i % 5 === 0) {

        console.log("FizzBuzz");

      } else {

        if (this.i % 3 === 0) {

          console.log("Fizz");

        } else {

          if (this.i % 5 === 0) {

            console.log("Buzz");

          } else {

            console.log(this.i);

          }
        }
      }
    }
  }
}

