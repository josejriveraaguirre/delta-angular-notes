//1.import component library from @angular/core
import { Component, OnInit } from "@angular/core";
//2.create the selector and template using the @Component class
@Component({
  selector: "app-fruit",
  // template: "<h3>Apple</h3>"
  templateUrl: "fruit.component.html",
  styleUrls: ["fruit.component.css"]
})

//3.export the component class
export class FruitComponent implements OnInit {

  ngOnInit()  {

  }
}
