import { Component, OnInit } from '@angular/core';
import {Comment} from "../../models/Comment";
import {CommentsService} from "../../services/comments.service";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  //PROPERTIES

  comments: Comment[] = [];

  selectedComment: Comment = {

    id: 0,
    name: "",
    email: "",
    body: ""

  }

  commentEdit: boolean = false

  constructor(private commentsService: CommentsService) { }

  ngOnInit(): void {

    this.commentsService.getComments().subscribe(data => {

      console.log(data);

      this.comments = data;

    })

  }

  // Requirement 6b: Create a form that has a button with a click event

  onSubmitComment(comment: Comment) {

    this.comments.unshift(comment);

  }

  onUpdatedComment(editComment: Comment) {

    this.comments.forEach((current, index) => {

      if (editComment.id === current.id) {

        this.comments.splice(index, 1);

        this.comments.unshift(editComment);

        this.commentEdit = false;

        this.selectedComment = {

          id: 0,
          name: "",
          email: "",
          body: ""

        }
      }
    })
  }

  editComment(comment: Comment) {

    this.selectedComment = comment;

    this.commentEdit = true;

  }

  removeComment(commentToBeDeleted: Comment) {

    if (confirm("Are you sure?")) {

      this.commentsService.deleteComment(commentToBeDeleted.id).subscribe(() => {

        this.comments.forEach((current, index) => {

          if (commentToBeDeleted.id === current.id) {

            this.comments.splice(index, 1);

          }

        })

      })

    }

  }

}
